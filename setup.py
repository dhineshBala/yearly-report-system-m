#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='PyModules', 
    version='1.0', 
    packages=find_packages(),
    install_requires=[
        'autopep8==1.4.4',
        'cycler==0.10.0',
        'kiwisolver==1.1.0',
        'matplotlib==3.1.1',
        'mplcursors==0.2.1',
        'numpy==1.17.0',
        'pandas==0.25.0',
        'pycodestyle==2.5.0',
        'pyparsing==2.4.2',
        'PySide2==5.13.0',
        'python-dateutil==2.8.0',
        'pytz==2019.2',
        'shiboken2==5.13.0',
        'six==1.12.0',
    ],
    )