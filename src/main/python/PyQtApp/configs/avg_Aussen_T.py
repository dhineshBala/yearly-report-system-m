from PyQtApp.src.common_class import CommonClass
import pandas as pd
import locale

E_AUSSEN_T = 'E_Aussen_T'

class AverageAussenTemp(CommonClass):

    def outside_temp_df(self):
        df_Aussen = self.create_df_from_key(E_AUSSEN_T)
        return df_Aussen

    def Outside_temp_avg_monthly(self):
        df = self.outside_temp_df()
        df.index = pd.to_datetime(df.index)

        daily = df.groupby(df.index.floor('d'))
        daily_maximum = daily['E_Aussen_T'].max()
        daily_minimum = daily['E_Aussen_T'].min()
        daily_sum = daily_maximum.add(daily_minimum, fill_value=0)
        
        daily_sum = daily_sum.reset_index()
        monthly = daily_sum.groupby(daily_sum.created_at.dt.strftime('%b %Y'))['E_Aussen_T'].sum().div(60)
        monthly = pd.DataFrame(monthly)
        monthly.index.name = 'Months'
        monthly = monthly.reset_index()
        monthly.Months = pd.to_datetime(monthly.Months, format='%b %Y').dt.strftime('%Y-%m')
        monthly = monthly.sort_values('Months')
        locale.setlocale(locale.LC_TIME, '')
        monthly.Months = pd.to_datetime(monthly.Months, format='%Y-%m').dt.strftime('%b %Y')
        monthly = monthly.set_index('Months')
        
        return df, monthly