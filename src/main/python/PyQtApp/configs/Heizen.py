from PyQtApp.src.common_class import CommonClass
import pandas as pd

class Heizen(CommonClass):
    def __init__(self, csv_name):
        CommonClass.__init__(self, csv_name)
        self._HZ_keys = self.read_json['keys']['Heizen']
        self._result_HZ_key = self.read_json['result_keys']['Heizen']

    def __clean_HZ_ST5bis12(self, df, key):
        """ This data cleaning of decimal 8 to 12 considering that (for one Heizperiode)
            each value shouldn't be greater than 5 times fo minimum value in this column """
        min_value = df[key].min()
        for index, row in df.iterrows():
            if row[key] > 5*min_value:
                df.at[index, key] = min_value
        return df

    def WMZ_Heizen_data(self):
        """ Filter the 'Heizen' data from the 'csv' file then clean and plot the data for 
            verification """
        df = self.filter_data_by_keys(self._HZ_keys)
        df = self.concat_sort_df(df)
        df = self.__clean_HZ_ST5bis12(df, self._HZ_keys[1])
        df = self.__clean_HZ_ST5bis12(df, self._HZ_keys[2])
        df = self.fill_df(df)
        # df.plot(marker='o')
        df[self._result_HZ_key] = self.calculate_result_df(df, self._HZ_keys)
        df = self.drop_big_values_df(df, self._result_HZ_key)
        df = self.drop_small_values_at_first_df(df, self._result_HZ_key)
        df = self.check_df_values(df, self._result_HZ_key)
        WMZ_HZ = self.df_monthly(df, self._result_HZ_key)
        return df, WMZ_HZ        
