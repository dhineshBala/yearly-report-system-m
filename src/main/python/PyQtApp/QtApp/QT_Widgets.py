from PySide2.QtCore import SLOT, QObject, Qt
from PySide2.QtWidgets import QApplication, QTableWidget, QTableWidgetItem, QLineEdit
from PySide2.QtCharts import QtCharts
from PySide2 import QtWidgets
import pandas as pd

from PyQtApp.latex.create_latex import ReportLatex
from PyQtApp.QtApp.QT_MsgHandler import ShowPythonException

class QtAppWidgets(QtWidgets.QWidget):
    def __init__(self):
        super(QtAppWidgets, self).__init__()
        self.table = QTableWidget()
        self._chart = QtCharts.QChart()
        self._chart_view = QtCharts.QChartView(self._chart)
        self.box = None
        self._axis_X = None
        
        self.SerialNumberInput = None
        self.TitleInput = None
        self.LastNameInput = None
        self.FirstNameInput = None
        self.StreetInput = None
        self.PlzAndCityInput = None

    def exit_app(self):
        QApplication.quit()

    def show_table(self):
        serialNumber = self.SerialNumberInput.text()
        try:
            df = ReportLatex(serialNumber).create_document()
            item = 0
            for index, row in df.iterrows():
                self.table.insertRow(item)
                self.table.setItem(item, 0, QTableWidgetItem(str(index)))
                self.table.setItem(item, 1, QTableWidgetItem(str(row['Heizen'])))
                self.table.setItem(item, 2, QTableWidgetItem(str(row['Warmwasser'])))
                item += 1
        except OSError as error:
            ShowPythonException(error)

    def clear_fields(self):
        self.table.setRowCount(0)
        self._chart.removeAllSeries()
        if self._axis_X is not None:
            self._chart.removeAxis(self._axis_X)
        self.SerialNumberInput.clear()
        self.TitleInput.clear()
        self.FirstNameInput.clear()
        self.LastNameInput.clear()
        self.StreetInput.clear()
        self.PlzAndCityInput.clear()

    def show_plot(self):
        self._chart.removeAllSeries()
        self._series = QtCharts.QBarSeries()
        self._axis_X = QtCharts.QBarCategoryAxis()
        for col in range(self.table.columnCount()):
            if not col == 0:
                _set = QtCharts.QBarSet(self.table.horizontalHeaderItem(col).text())
                for row in range(self.table.rowCount()):
                    _set.append(float(self.table.item(row, col).text()))
                self._series.append(_set)

        self._chart.addSeries(self._series)
        self._chart.legend().setAlignment(Qt.AlignTop)
        self._chart.setTitle('Warmemenge Overview')

        for x in range(self.table.rowCount()):
            self._axis_X.append([self.table.item(x, 0).text()])
        self._chart.createDefaultAxes()
        self._chart.setAxisX(self._axis_X, self._series)
        self._chart_view.setChart(self._chart)

    def add_QLineEdit(self, placeholderText):
        self.box = QLineEdit()
        self.box.setPlaceholderText(placeholderText)
        return self.box

    def serialNumber_QLE(self):
        self.SerialNumberInput = self.add_QLineEdit('W12345678')
        return self.SerialNumberInput

    def title_QLE(self):
        self.TitleInput = self.add_QLineEdit('Herr/Frau')
        return self.TitleInput

    def firstName_QLE(self):
        self.FirstNameInput = self.add_QLineEdit('Maxx')
        return self.FirstNameInput

    def lastName_QLE(self):
        self.LastNameInput = self.add_QLineEdit('Muster')
        return self.LastNameInput

    def Street_QLE(self):
        self.StreetInput = self.add_QLineEdit('Straße und Nr')
        return self.StreetInput

    def PlzAndCity_QLE(self):
        self.PlzAndCityInput = self.add_QLineEdit('PLZ und Ort')
        return self.PlzAndCityInput
    


    
    
