from PyQtApp.configs.Warmwasser import WarmWasser
from PyQtApp.configs.Heizen import Heizen
from PyQtApp.configs.avg_Aussen_T import AverageAussenTemp
from PyQtApp.configs.Taktungen import TaktungenVD1
import pandas as pd
import locale, os
from time import strftime
import matplotlib.pyplot as plt

class BarChartGroup(WarmWasser, Heizen, AverageAussenTemp, TaktungenVD1):
    def __init__(self, csv_name):
        WarmWasser.__init__(self, csv_name)
        Heizen.__init__(self, csv_name)
        self.plot_dir = os.path.join(self.path_to_dir, 'Plots')

    def dataframe_from_dicts(self):
        df_WW, dict_WW = self.WMZ_Warmwasser_data()
        df_HZ, dict_HZ = self.WMZ_Heizen_data()
        df_Aussen, df_Aussen_avg_month = self.Outside_temp_avg_monthly()
        df_Taktungen = self.create_monthly_taktungen_df()
        
        # create csv for further reference
        df = pd.concat([df_Aussen, df_HZ, df_WW], sort=False)
        df = df.sort_index()
        df.to_csv(self.path_to_dir + '/cleaned_Data/' + self.csv_name + ".csv")

        # With outside temperatures
        # df = pd.concat([df_Aussen, df_HZ, df_WW], sort=False)
        # df = self.fill_df(df)
        # df = df.sort_index()
        # # plt.scatter([x for x in df.index], df.E_Aussen_T, c='blue', s=0.5)
        # # plt.scatter([x for x in df.index], df.WMZ_HZ, c='black', s=2)
        # # plt.scatter([x for x in df.index], df.WMZ_WW, c='grey', s=1)
        # # plt.show()
        # df.to_csv('cleaned_Data/' + self.csv_name + "_Aussen_T.csv")

        # creating 'Dataframe' from dict values
        df = pd.DataFrame([dict_HZ, dict_WW])
        df = df.transpose()
        df = df.rename(columns={0:'Heizen', 1:'Warmwasser'})
        df = df.sort_index()
        df.index.name = 'Months'

        kWh_Heizen = df.sum(axis=0)['Heizen']
        kWh_Warmwasser = df.sum(axis=0)['Warmwasser']
        
        # 'Reindexing' the Dataframe for dropping the 'Null' rows
        df_reset = df.reset_index()
        for index, row in df_reset.iterrows():
            if (row['Heizen'] == 0 or  pd.isnull(row['Heizen'])) and (row['Warmwasser'] == 0 or  pd.isnull(row['Warmwasser'])):
                df_reset.drop(index, inplace=True)
        # Get local date format for the final bar chart
        locale.setlocale(locale.LC_TIME, '')
        df_reset['Months'] = pd.to_datetime(df_reset.Months, format='%Y-%m', errors='coerce').dt.strftime('%b %Y')
        df = df_reset.set_index('Months')
        
        # Group plot of Warmwasser and Heizen 'Warmemenge'
        fig_length = len(df.index)
        ax = df.plot.bar(rot=0, color=['black', 'grey'], title='Erzeugte Wärmemengen für Heizperiode - 2018/2019', zorder=10, figsize=(fig_length,fig_length/2))
        ax.set_xlabel('')
        ax.set_ylabel('Wärmemengen [kWh]')
        ax.grid('on', which='major', axis='x', zorder=1 )
        ax.grid('on', which='major', axis='y', zorder=2 )
        manager = plt.get_current_fig_manager()
        if fig_length > 8:
            manager.window.state('zoomed')
        # manager.resize(*manager.window.maxsize())
        # manager.full_screen_toggle()
        plt.savefig(self.plot_dir + "/" + self.csv_name + "_BarChart.png", bbox_inches='tight', dpi=300)

        pieLabels = ['Heizen', 'Warmwasser']
        sizes = [kWh_Heizen, kWh_Warmwasser]
        fig1, ax1 = plt.subplots()
        pie_title = "Verhältnis Heizbetrieb zu Warmwasserbereitung Ihres System M [%]"
        patches, text, autotexts = ax1.pie(sizes, labels=None, autopct='%1.1f%%', colors=['black', 'grey'], startangle=90)
        ax1.axis('equal')
        for autotext in autotexts:
            autotext.set_color('white')
        handles, labels = ax1.axes.get_legend_handles_labels()
        ax1.legend(loc=3, labels=pieLabels)
        plt.title(pie_title)
        plt.savefig(self.plot_dir + "/" + self.csv_name + "_PieChart.png", bbox_inches='tight', dpi=300)
        
        df = pd.concat([df, df_Aussen_avg_month, df_Taktungen], axis=1)
        return df, kWh_Heizen, kWh_Warmwasser
    
