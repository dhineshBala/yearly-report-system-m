import json, os

class UserInput(object):
    def __init__(self):
        with open(os.path.join(os.path.dirname(__file__),'info.json')) as info:
            json_info = json.load(info)
        self.serialNumbers = json_info['serialnumbers']
        self.keys = json_info['keys']

    def get_user_input(self):
        print("List of available CSV files")
        for sn in self.serialNumbers:
            print(sn)
        u_sn = input("Enter serialnumber from the list: ")
        
        for sn in self.serialNumbers:
            if u_sn == sn:
                print("You have entered:", sn)
                confirm = input("To continue[y]: ")
                if confirm == 'y':
                    return sn
                else:
                    print("See you next time. Bye!")
                    break
        else:
            print("The entered serialnumber is not available. Please try again!")


if __name__ == "__main__":
    WMZ = UserInput()
    u_input = WMZ.get_user_input()
    if u_input is not None:
        cwd = os.getcwd()
        csv_name = u_input 

        # from configs.Heizen import Heizen
        # from configs.Warmwasser import WarmWasser

        # Hz = Heizen(cwd, csv_file)
        # Hz.WMZ_Heizen_data()
        # Hz.plot_show()

        from latex.create_latex import ReportLatex
        from Excel_files.create_excel import CreateExcel
        
        # lat = ReportLatex(cwd, csv_name)
        # lat.create_document()
        # lat.plot_show()

        xl = CreateExcel(cwd, csv_name)
        xl.plot_show()

        
        
        

    
    