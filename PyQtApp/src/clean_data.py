import pandas as pd
from itertools import islice

class CleanData(object):

    def generate_empty_df(self, base_df, empty_df, column_name):
        if empty_df.empty:
            empty_df = pd.DataFrame({column_name: 0}, index=base_df.index.copy())
            empty_df = self.convert_to_pd_datetime(empty_df)
            return empty_df
        else:
            empty_df.rename(columns={'value_new': column_name}, inplace=True)
            empty_df.drop(['key'], axis=1, inplace=True)
            empty_df = self.convert_to_pd_datetime(empty_df)
            empty_df = empty_df.sort_index()
            return empty_df
    
    def check_df_values(self, df, colName):
        """ verifies the values in dataframe if its valid or not in progressive order """
        count = 0
        for index, row in df.iterrows():
            if count == 0:
                old_value = row[colName]
            else:
                if row[colName] < old_value:
                    df.at[index, colName] = old_value
                old_value = row[colName]
            count += 1
        
        # self.plot_to_check(df, colName)
        return df

    def clean_WMZ_ST5bis8_values(self, df, colName):
        """
        Drop 'rows' when there is too much increament in the timeseries data
        2018-08-22 10:32:31,8805530001.0,2248.0,1.0,0.0
        2018-08-22 10:41:11,8805530001.0,2249.0,1.0,0.0
        2018-08-22 10:41:11,8805530001.0,2249.0,1.0,0.0
        # 2018-08-22 14:22:12,8805530001.0,2249.0,2249.0,0.0
        # 2018-08-22 14:22:31,8805530001.0,2249.0,2249.0,0.0
        """
        count = 0
        for index, row in df.iterrows():
            if not pd.isna(row[colName]):
                if count == 0:
                    old_value = row[colName]
                else:
                    if row[colName] > (2 * old_value + 2):
                        df.at[index, colName] = old_value
                    old_value = row[colName]
                count += 1
        return df

    def fill_df(self, df):
        df = df.ffill().bfill()
        return df

    def drop_big_values_df(self, df, colName):
        """ Drop 'rows' when there is a high value in the middle of df when compared to the last value"""
        """ 2019-04-07 02:24:01,8805530001,9998.0,1.0,0.0,19998.0
            2019-04-07 02:49:32,8805530001,9999.0,1.0,0.0,19999.0
            # 2019-04-07 03:22:50,8805530001,9999.0,2.0,0.0,29999.0
            # 2019-04-07 03:22:50,8805530001,9999.0,2.0,0.0,29999.0
            2019-04-07 04:03:02,8805530001,1.0,2.0,0.0,20001.0
            2019-04-07 04:26:53,8805530001,2.0,2.0,0.0,20002.0
            2019-04-07 04:35:39,8805530001,3.0,2.0,0.0,20003.0
            """
        last_value = df[colName].iloc[-1]
        df_reset = df.reset_index() 
        for index, row in df_reset.iterrows():
            if row[colName] > last_value:
                df_reset.drop(index, inplace=True)
        df = df_reset.set_index('created_at', drop=True)
        return df

    def drop_small_values_at_first_df(self, df, colName):
        df_10_rows = df.head(10)
        df_reset = df.reset_index()
        max_value = df_10_rows[colName].max()
        for index, row in islice(df_reset.iterrows(), 10):
            if 10 + row[colName] < max_value:
                df_reset.drop(index, inplace=True)
        df = df_reset.set_index('created_at', drop=True)
        return df
                 


    