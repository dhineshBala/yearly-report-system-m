from PyQtApp.src.clean_data import CleanData
from PyQtApp.src.plots import ResultPlots
import pandas as pd
import os, json
from time import strftime
import PyQtApp

class CommonClass(CleanData, ResultPlots):
    def __init__(self, csv_name):
        self.path_to_dir = PyQtApp.wd_path
        csv_loc = os.path.join(self.path_to_dir, 'Data')
        csv_path = os.path.join(csv_loc, csv_name + ".csv")
        self._from_csv = pd.read_csv(csv_path, index_col='created_at')
        self.csv_name = csv_name
        
        json_path = os.path.join(self.path_to_dir, 'info.json')
        with open(json_path) as info:
            self.read_json = json.load(info)

    def _print_head(self, df):
        print(df.head())
    
    def _print_tail(self, df):
        print(df.tail())

    def _convert_to_csv(self, df, csv_name):
        df.to_csv(csv_name)

    def convert_to_pd_datetime(self, df):
        df.index = pd.to_datetime(df.index)
        return df

    def concat_sort_df(self, df):
        df = pd.concat(df, sort=False)
        df = df.sort_index()
        return df

    def filter_data_by_keys(self, keys):
        count = 0
        df = []
        for key in keys:
            df_new = self._from_csv[self._from_csv['key'] == key]
            if count == 0:
                df_old = df_new
            df_new = self.generate_empty_df(df_old, df_new, key)
            df_sub = self.check_df_values(df_new, key)
            df.append(df_sub)
            count += 1
        return df
    
    def create_df_from_key(self, key):
        df = self._from_csv[self._from_csv['key'] == key]
        df.rename(columns={'value_new': key}, inplace=True)
        df.drop(['key'], axis=1, inplace=True)
        df = self.convert_to_pd_datetime(df)
        df = df.sort_index()
        return df

    def df_monthly(self, df, colName):
        dict = {}
        for i in range(1,13):
            monthly = df.loc[df.index.month == i, :]
            
            try:
                MonthYear = monthly.index[-1].strftime('%Y-%m')
                dict[MonthYear] = monthly[colName].iloc[-1] - monthly[colName].iloc[0]
            except:
                pass
        return dict

    # def group_df_monthly(self, df):
    #     df.index = pd.to_datetime(df.index)
    #     monthly = df.groupby(pd.Grouper(freq='m'))
    #     print(monthly.first())
        
        # for i in range(1,13):
        #     monthly = df.loc[df.index.month == i, :]
            
        #     try:
        #         MonthYear = monthly.index[-1].strftime('%Y-%m')
        #         dict[MonthYear] = monthly[colName].iloc[-1] - monthly[colName].iloc[0]
        #     except:
        #         dict[str(i)] = 0
        # return dict
        
    def calculate_result_df(self, df, keys):
        """ calculate ST_1bis4 + 10**4 * ST5bis8 + 10**8 * ST9bis12 """
        result_df = df[keys[0]] + (10**4 * df[keys[1]]) + (10**8 * df[keys[2]])
        return result_df


            

        
