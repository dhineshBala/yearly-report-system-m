from matplotlib import pyplot as plt
import mplcursors

class ResultPlots(object):
    def plot_to_check(self, x, y, title):
        plt.figure(title)
        plt.plot(x, y, 'o-')
        plt.title(title)
        # Values on hovering plot for verification
        mplcursors.cursor(hover=True)

    def bar_chart(self, dict, title):
        plt.figure(title)
        plt.bar(range(len(dict)), list(dict.values()), align='center')
        plt.xticks(range(len(dict)), list(dict.keys()))

    def plot_show(self):
        plt.show()

    # def scatter_plot(self, df, *colNames):
    #     colors = ("red", "green", "blue")
        
    #     s = plt.figure()
    #     ax = s.add_subplot(1, 1, 1, axisbg='1.0')

    #     for data, color, group in zip(df[*colNames], colors, *colNames):
    #         x, y = data
    #         ax.scatter(x, y, alpha=0.8, c=color, edgecolors='none', s=30, label=group)

    #     plt.title('Scatter')
    #     plt.legend(loc=2)
    #     plt.show()





