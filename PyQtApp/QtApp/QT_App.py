from PyQtApp.QtApp.QT_LeftWidgetsForm import QtAppLeftWidgets
from PyQtApp.QtApp.QT_RightShowResults import QtAppRightResults
from PySide2.QtWidgets import QHBoxLayout, QVBoxLayout

class QtCustomerApp(QtAppLeftWidgets, QtAppRightResults):
    def __init__(self):
        super(QtCustomerApp, self).__init__()
        self.layout = QHBoxLayout()
        self.set_left_layout()
        self.set_right_layout()
        self.layout.addLayout(self.left)
        self.layout.addLayout(self.right)
        self.setLayout(self.layout)

    def set_left_layout(self):
        self.QTextBox_view()
        self.QCalender_view()
        self.QPushButton_view()

    def set_right_layout(self):
        self.QTableWidget_view()
        self.QChart_view()