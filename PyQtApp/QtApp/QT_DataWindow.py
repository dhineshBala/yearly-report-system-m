from PySide2.QtCore import Slot, qApp
from PySide2.QtGui import QKeySequence
from PySide2.QtWidgets import QMainWindow, QAction
from PySide2 import QtCore
from PyQtApp.QtApp.QT_Widgets import QtAppWidgets

class QtMainWindow(QMainWindow, QtAppWidgets):
    def __init__(self, widget):
        super(QtMainWindow, self).__init__()

        self.setWindowTitle("Jährliche Anlagenberichte System-M")
        self.setCentralWidget(widget)

        # Menu
        self.menu = self.menuBar()
        self.file_menu = self.menu.addMenu("File")

        # New QAction
        new_action = QAction("New", self)
        new_action.setShortcut(QKeySequence.New)
        new_action.triggered.connect(self.create)

        # Save as QAction
        save_action = QAction("Save as PDF", self)
        save_action.setShortcut(QKeySequence.Save)
        save_action.triggered.connect(self.saveState)

        # Exit QAction
        exit_action = QAction("Exit", self)
        exit_action.setShortcut(QKeySequence.Quit)
        exit_action.triggered.connect(self.exit_app)

        
        self.file_menu.addAction(new_action)
        self.file_menu.addAction(save_action)
        self.file_menu.addAction(exit_action)

        # Status Bar
        self.status = self.statusBar()
        self.status.showMessage("Application is ready")

        # Window dimensions
        self.setWindowState(self.windowState())
       