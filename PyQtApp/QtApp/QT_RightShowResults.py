from PySide2.QtWidgets import QTableWidget, QHeaderView, QVBoxLayout, QTableWidgetItem
from PySide2 import QtWidgets
from PySide2.QtCharts import QtCharts
from PySide2.QtCore import Qt, QObject
from PySide2.QtGui import QPainter
from PyQtApp.QtApp.QT_Widgets import QtAppWidgets

class QtAppRightResults(QtAppWidgets):
    def __init__(self):
        super(QtAppRightResults, self).__init__()
        self.right = QVBoxLayout()
        self.right.setMargin(10)

    def QTableWidget_view(self):
        self.table.setColumnCount(3)
        self.table.setHorizontalHeaderLabels(
            ['Months', 'Heizen WM', 'Warmwasser WM'])
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.right.addWidget(self.table)

    def QChart_view(self):
        # Creating Charts view
        self._chart.setAnimationOptions(QtCharts.QChart.AllAnimations)
        self._chart_view.setRenderHint(QPainter.Antialiasing)
        self.right.addWidget(self._chart_view)

    
        
    

        

