import os, sys
from PySide2 import QtCore
from PySide2.QtWidgets import QDialog, QWidget, QMessageBox

class ShowWarningBox(QWidget):
    def __init__(self):
        super(ShowWarningBox, self).__init__()

    def QDialog_show(self, mode, context, message):
        Info_box = QMessageBox()
        Info_box.setIcon(getattr(Info_box, mode))
        Info_box.setWindowTitle(mode)
        Info_box.setText("%s (%s:%d, %s)" % (message, context.file, context.line, context.file))
        Info_box.exec_()

def QtMsghandler(mode, context, message):
    if mode == QtCore.QtInfoMsg:
        mode = 'Information'
    elif mode == QtCore.QtWarningMsg:
        mode = 'Warning'
    elif mode == QtCore.QtCriticalMsg:
        mode = 'Critical'
    elif mode == QtCore.QtFatalMsg:
        mode = 'Critical'
    else:
        mode = 'NoIcon'
    ShowWarningBox().QDialog_show(mode, context, message)

class ShowPythonException(QWidget):
    def __init__(self, error):
        super(ShowPythonException, self).__init__()
        self.show_PyException(error)

    def show_PyException(self, error):
        Info_box = QMessageBox()
        Info_box.setIcon(Info_box.Critical)
        Info_box.setWindowTitle('Python Error')
        Info_box.setText("%s" % (error))
        Info_box.exec_()
