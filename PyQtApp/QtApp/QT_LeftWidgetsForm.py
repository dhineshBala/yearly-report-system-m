import sys
import os
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtWidgets import QHBoxLayout, QVBoxLayout, QSizePolicy, QCalendarWidget, QHeaderView, \
    QLabel, QDialog
from PySide2.QtCore import QDate
from PyQtApp.QtApp.QT_Widgets import QtAppWidgets

class QtAppLeftWidgets(QtAppWidgets):
    def __init__(self):
        super(QtAppLeftWidgets, self).__init__()
        self.left = QVBoxLayout()
        self.left.setMargin(10)

    def QTextBox_view(self):
        self.left.addWidget(QLabel('Seriennummer:'))
        self.left.addWidget(self.serialNumber_QLE())
        self.left.addWidget(QLabel('Anrede:'))
        self.left.addWidget(self.title_QLE())
        self.left.addWidget(QLabel('Nachname:'))
        self.left.addWidget(self.lastName_QLE())
        self.left.addWidget(QLabel('Vorname:'))
        self.left.addWidget(self.firstName_QLE())
        self.left.addWidget(QLabel('Anschrift:'))
        self.left.addWidget(self.Street_QLE())
        self.left.addWidget(self.PlzAndCity_QLE())

    def QCalender_view(self):
        self.left_Date_view = QHBoxLayout()
        self.QCalender_from_view()
        self.left_Date_view.addLayout(self.left_Date_left_view)
        self.QCalender_to_view()
        self.left_Date_view.addLayout(self.left_Date_right_view)
        self.left.addLayout(self.left_Date_view)

    def QCalender_from_view(self):
        self.left_Date_left_view = QVBoxLayout()
        self.left_Date_left_view.addWidget(QLabel('From Date:'))
        calender = QCalendarWidget()
        calender.setDateRange(QDate(2019, 6, 1), QDate.currentDate())
        calender.setGridVisible(True)
        self.left_Date_left_view.addWidget(calender)

    def QCalender_to_view(self):
        self.left_Date_right_view = QVBoxLayout()
        self.left_Date_right_view.addWidget(QLabel('To Date:'))
        calender = QCalendarWidget()
        calender.setDateRange(QDate(2019, 6, 1), QDate.currentDate())
        calender.setGridVisible(True)
        self.left_Date_right_view.addWidget(calender)

    def QPushButton_view(self):
        def QPushButton_CSS(PushButton):
            PushButton.setFont(QtGui.QFont('Times', 14, QtGui.QFont.Bold))
            return PushButton

        show_table = QPushButton_CSS(QtWidgets.QPushButton("Show Table"))
        show_plot = QPushButton_CSS(QtWidgets.QPushButton("Show Plot"))
        clear_all = QPushButton_CSS(QtWidgets.QPushButton("Clear All"))
        exit = QPushButton_CSS(QtWidgets.QPushButton("Quit"))

        self.connect(show_table, QtCore.SIGNAL("clicked()"), self.show_table)
        self.connect(show_plot, QtCore.SIGNAL("clicked()"), self.show_plot)
        self.connect(clear_all, QtCore.SIGNAL("clicked()"), self.clear_fields)
        self.connect(exit, QtCore.SIGNAL("clicked()"), self.exit_app)

        self.left.addSpacing(50)
        self.left.addWidget(show_table)
        self.left.addWidget(show_plot)
        self.left.addWidget(clear_all)
        self.left.addWidget(exit)