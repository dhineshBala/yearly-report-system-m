from fbs_runtime.application_context.PySide2 import ApplicationContext
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import qInstallMessageHandler
from PyQtApp.QtApp.QT_MsgHandler import QtMsghandler
import sys

from PyQtApp.QtApp.QT_DataWindow import QtMainWindow
from PyQtApp.QtApp.QT_App import QtCustomerApp

if __name__ == "__main__":
    # appctxt = ApplicationContext()
    qInstallMessageHandler(QtMsghandler)
    app = QApplication(sys.argv)
    widget = QtCustomerApp()
    window = QtMainWindow(widget)
    window.resize(1000,800)
    window.show()
    sys.exit(app.exec_())
    # exit_code = appctxt.app.exec_()
    # sys.exit(exit_code)

