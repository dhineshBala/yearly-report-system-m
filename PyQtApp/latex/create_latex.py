from PyQtApp.src.bar_chart import BarChartGroup
import os
import json
from pylatex import Document, PageStyle, Head, Foot, MiniPage, \
    StandAloneGraphic, MultiColumn, Tabu, LongTabu, LargeText, MediumText, SmallText, \
    TextColor, LineBreak, NewPage, Tabularx, TextColor, simple_page_number, UnsafeCommand, Command, Tabular, VerticalSpace, HorizontalSpace, \
    HFill, Itemize, Hyperref

from pylatex.base_classes import CommandBase, Arguments, Environment
from pylatex.package import Package
from pylatex.utils import bold, NoEscape
from itertools import islice


class changePageWidth(Environment):
    _latex_name = 'adjustwidth'
    packages = [Package('changepage')]


class ReportLatex(BarChartGroup):
    def __init__(self, csv_name):
        BarChartGroup.__init__(self, csv_name)

    def create_document(self):

        df, kWh_Heizen, kWh_Warmwasser = self.dataframe_from_dicts()
        from_month = df.index[0]
        to_month = df.index[-1]
        total_WMZ = kWh_Heizen + kWh_Warmwasser

        latex_dir = os.path.dirname(__file__)

        with open(os.path.join(latex_dir, 'Report_info.json'), encoding='utf-8') as report:
            report_tex = json.load(report)
        Footer = report_tex["Footer"]
        SideFooter = Footer["Side_footer"]
        FirstPage = report_tex["First_page"]

        geometry_options = {
            "paper": "a4paper",
            "head": "70pt",
            # "margin": "1in",
            "top": "1mm",
            # "bottom": "25.4mm",
            "right": "0mm",
            # "footheight": "0.5in",
            # "footskip": "10pt",
            "includeheadfoot": True
        }
        doc = Document(geometry_options=geometry_options)
        doc.packages.append(Package('babel', options='ngerman'))
        doc.packages.append(Package('parskip', options='parfill'))

        sty_first_page = PageStyle('firstpage')

        logo_path = os.path.join(latex_dir, 'logo/logo.png')
        logo_path = logo_path.replace(os.sep, '/')

        with sty_first_page.create(Head("R")) as rightHead:
            rightHead.append(Command('flushright'))
            rightHead.append(StandAloneGraphic(
                image_options='width=0.22\linewidth', filename=logo_path))

        with sty_first_page.create(Foot("C")) as footer:
            footer.append(Command('tiny'))
            with footer.create(Tabularx("X X X", width_argument=NoEscape(r"\textwidth"))) as footer_table:
                footer_table.add_hline(color="black")
                footer_table.add_empty_row()
                contact_info = MiniPage(width=NoEscape(
                    r"0.25\textwidth"), pos='t', content_pos='l', align='l')
                contact = SideFooter['Address']
                contact_info.append(bold(contact[0]))
                contact_info.append(LineBreak())
                for address in islice(contact, 1, None):
                    contact_info.append(address)
                    contact_info.append(LineBreak())

                bank_info = MiniPage(width=NoEscape(
                    r"0.25\textwidth"), pos='t', content_pos='l', align='l')
                bank_info.append(bold("Bankverbindung:"))
                bank_info.append(LineBreak())
                for bank in SideFooter['Account']:
                    bank_info.append(bank)
                    bank_info.append(LineBreak())

                service_info = MiniPage(width=NoEscape(
                    r"0.25\textwidth"), pos='t', content_pos='l', align='l')
                service_info.append(bold("Kundenservice:"))
                service_info.append(LineBreak())
                for service in SideFooter['Service']:
                    service_info.append(service)
                    service_info.append(LineBreak())
                service_info.append(Command('flushright'))
                service_info.append(
                    NoEscape(r'Seite \thepage\ von \pageref{LastPage}'))

                footer_table.add_row([contact_info, bank_info, service_info])

        doc.preamble.append(sty_first_page)

        doc.append(VerticalSpace('5mm'))
        with doc.create(MiniPage(width=NoEscape(r'0.69\textwidth'), pos='h', align='l')) as company_info:
            company_info.append(Command('scriptsize'))
            for address in islice(SideFooter['Address'], 1, 4):
                company_info.append(address + " ")

        with doc.create(MiniPage(width=NoEscape(r'0.29\textwidth'), pos='h', align='c')) as date_info:
            date_info.append(Command('scriptsize'))
            date_info.append(NoEscape(r'\today'))
            date_info.append(LineBreak())
            # date_info.append("Kulmbach")
            # date_info.append(LineBreak())

        doc.append(LineBreak())
        doc.append(LineBreak())
        doc.append(Command('normalsize'))
        doc.append(Command('flushleft'))
        customer_info = FirstPage['Customer_info']
        for key, values in customer_info.items():
            if key == 'Address':
                for customer_address in values:
                    doc.append(LineBreak())
                    doc.append(customer_address)
                break
            doc.append(values + " ")
        doc.append(LineBreak())
        doc.append(VerticalSpace('0.6in'))
        doc.append(LineBreak())

        doc.change_document_style("firstpage")

        with doc.create(changePageWidth(arguments=Arguments('0pt', '49pt'))) as body:
            body.append(LargeText(bold("Jährlicher Anlagenbericht")))
            body.append(LineBreak())
            body.append(LineBreak())

            body.append(
                "Sehr geehrter " + customer_info["Title"] + " " + customer_info["LastName"] + ",")
            body.append(LineBreak())

            body.append(LineBreak())
            body.append(FirstPage["First_Paragraph"])
            body.append(LineBreak())

            body.append(LineBreak())
            body.append(FirstPage["Second_Paragraph"])
            with body.create(Itemize()) as itemize:
                for action in FirstPage["Actions"]:
                    itemize.add_item(action)
            body.append(FirstPage["Second_Paragraph_cont"])
            body.append(LineBreak())

            body.append(LineBreak())
            body.append(bold("Unsere Empfehlung:"))
            body.append(LineBreak())
            body.append(FirstPage['Feedback'])
            body.append(LineBreak())
            body.append(LineBreak())

            body.append(FirstPage['Questions?'])
            body.append(LineBreak())

            body.append(LineBreak())
            body.append("Mit freundlichen Grüßen")
            body.append(LineBreak())
            body.append(bold("Ihr GDTS Team"))
            body.append(VerticalSpace('4mm'))
            body.append(LineBreak())

            body.append(SmallText(FirstPage['Post_Script']))
            # body.append(LineBreak())
            # body.append(LineBreak())

            body.append(SmallText(FirstPage['More_info']))
            # body.append(LineBreak())

            body.append(NewPage())

            body.append(LargeText(bold("System M – Anlagenbericht")))
            body.append(LineBreak())
            body.append(LineBreak())

            with body.create(Tabular('|c|c|')) as table:
                table.add_hline()
                table.add_empty_row()
                table.add_row("Serialnummer: " + str(self.csv_name),
                              "Betrachtungszeitraum: " + str(from_month) + " bis " + str(to_month))
                table.add_empty_row()
                table.add_hline()
                table.add_empty_row()
                table.add_row("Erzeugte Wärmemenge: " +
                              str(int(total_WMZ)) + " kWh", " ")
                table.add_empty_row()
                table.add_hline()

            SecondPage = report_tex['Second_page']

            body.append(LineBreak())
            body.append(LineBreak())
            body.append(SecondPage["First_Paragraph"])
            body.append(LineBreak())
            body.append(LineBreak())

            body.append(SecondPage["Second_Paragraph"])
            body.append(LineBreak())
            body.append(LineBreak())

            bar_chart = os.path.join(
                self.plot_dir, self.csv_name + '_BarChart.png')
            pie_chart = os.path.join(
                self.plot_dir, self.csv_name + '_PieChart.png')
            bar_chart = bar_chart.replace(os.sep, '/')
            pie_chart = pie_chart.replace(os.sep, '/')

            body.append(Command('center'))
            body.append(StandAloneGraphic(
                image_options="width=0.9\linewidth", filename=bar_chart))

            body.append(LineBreak())
            body.append(Command('flushleft'))

            count = 0
            for detail in SecondPage["Explanation"]:
                if count == 1:
                    body.append(str(int(total_WMZ)) + " ")
                if count == 2:
                    body.append(str(int(kWh_Heizen)) + " ")
                if count == 3:
                    body.append(str(int(kWh_Warmwasser)) + " ")
                body.append(detail)
                count += 1

            body.append(NewPage())
            body.append(Command('center'))
            body.append(StandAloneGraphic(
                image_options="width=0.5\linewidth", filename=pie_chart))
            body.append(LineBreak())

            body.append(Command('flushleft'))
            body.append(SecondPage['End_Paragraph'])
            body.append(LineBreak())
            body.append(LineBreak())
            body.append(SecondPage['End'])
            body.append(LineBreak())

        doc.generate_pdf(latex_dir + '/Reports/' + self.csv_name + '_Yearly_Report_18-19', clean_tex=False,
                         compiler='pdflatex')
        
        return df
