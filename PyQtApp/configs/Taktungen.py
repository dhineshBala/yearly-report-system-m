from PyQtApp.src.common_class import CommonClass
import pandas as pd
import locale

keys = ['BS_Vd1_SV_H',
        'BS_Vd1_SV_K',
         'BS_Vd1_SV_WW']

class TaktungenVD1(CommonClass):

    def create_monthly_taktungen_df(self):
        operation_dicts = []
        for operation in keys:
            df = self.create_df_from_key(operation)
            df = self.drop_small_values_at_first_df(df, operation)
            df = self.check_df_values(df, operation)
            dict = self.df_monthly(df, operation)
            operation_dicts.append(dict)
        df = pd.DataFrame(operation_dicts)
        df = df.transpose()
        df = df.rename(columns={0:'Tahtungen_HZ', 1:'Taktungen_KL', 2:'Taktungen_WW'})

        df = df.sort_index()
        df.index.name = 'Months'

        # Get local date format for the final bar chart
        locale.setlocale(locale.LC_TIME, '')
        df.reset_index(inplace=True)
        df.Months = pd.to_datetime(df.Months, format='%Y-%m', errors='coerce').dt.strftime('%b %Y')
        df = df.set_index('Months')
        return df


    
    
