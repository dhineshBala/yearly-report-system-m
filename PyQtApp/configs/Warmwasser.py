from PyQtApp.src.common_class import CommonClass

class WarmWasser(CommonClass):
    def __init__(self, csv_name):
        CommonClass.__init__(self, csv_name)
        self._WW_keys = self.read_json['keys']['Warmwasser']
        self._result_WW_key = self.read_json['result_keys']['Warmwasser']
    
    def WMZ_WW_result_df(self, df):
        calc = self.calculate_result_df(df, self._WW_keys)
        df[self._result_WW_key] = calc
        return df

    def WMZ_Warmwasser_data(self):
        df = self.filter_data_by_keys(self._WW_keys)
        df = self.concat_sort_df(df)
        df = self.clean_WMZ_ST5bis8_values(df, self._WW_keys[1])
        df = self.fill_df(df)
        df = self.WMZ_WW_result_df(df)
        df = self.check_df_values(df, self._result_WW_key)
        WMZ_WW = self.df_monthly(df, self._result_WW_key)
        return df, WMZ_WW

    


    

        
